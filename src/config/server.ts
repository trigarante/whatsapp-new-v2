
import * as express from "express";
// import { SERVER_PORT } from '../global/environment';
import * as socketIO from 'socket.io';
import * as http from 'http';

import * as socket from '../sockets/socket';



export default class Server {

    private static _intance: Server;

    public app: express.Application;
    public port: any;

    public io: socketIO.Server;
    private httpServer: http.Server;


    private constructor() {

        this.app = express();
        this.port = process.env.PORT || 8080;

        this.httpServer = new http.Server( this.app );
        this.io = socketIO( this.httpServer, {
            cors: {
                transports : ['websocket','polling', 'flashsocket'],
                origins: ["https://trigarante2020.com/", "https://wavy.mark-43.net/", "http://localhost:4200/"],
                methods: ['GET'],
                extraHeaders: {
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Headers": "Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method"
                },
                secure: false,
                rejectUnauthorized: false
            }
        } );

        this.start( callback => {
            console.log('Conectadoooooooooooo');
            this.escucharSockets();
        });
    }

    public static get instance() {
        return this._intance || ( this._intance = new this() );
    }


    private escucharSockets() {

        console.log('Escuchando conexiones - sockets');

        let idSolicitud = '';
        let idEmpleado = '';

        this.io.on('connection', cliente => {

            idSolicitud = cliente.handshake.query.idSolicitud;
            idEmpleado = cliente.handshake.query.idEmpleado;
// se genera una sala
// console.log(idSolicitud === null, idSolicitud, typeof(idSolicitud));
            if (!idSolicitud || idSolicitud === 'null') {
                // console.log('se ha conectado un usuario a solicitudes', ` room_solicitudes_${idEmpleado}`);
                cliente.join(`room_solicitudes_${idEmpleado}`);
            } else {
                // console.log('se ha conectado un usuario');
                cliente.join(`room_${idSolicitud}`);
            }

            // Mensajes
            socket.mensaje( cliente, this.io );

            // Desconectar
            socket.desconectar( cliente );         

        });

    }


    start( callback: Function ) {

        this.httpServer.listen( this.port, callback );

    }

}
