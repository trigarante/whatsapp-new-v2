import { Router } from "express";
import whatsapp from "./Whatsapp/whatsapp";
import plantillasWhatsApp from './Whatsapp/PlantillasWhatsapp/plantillasWhatsapp';
const routes = Router();

routes.use("/", whatsapp);
routes.use('/plantillas-comercial-whatsapp', plantillasWhatsApp);

export default routes;
