import { Router } from 'express';

import PlantillasWhatsappController
    from '../../../controllers/whatsapp/PlantillasWhatsapp/PlantillasWhatsappController';
const routes: Router = Router();

routes.post('/agregar-plantilla', PlantillasWhatsappController.nuevaPlantilla);
routes.get('/obtener-plantilla/:idPlantilla', PlantillasWhatsappController.getPlantillaWhatsapp);
routes.get('/obtener-plantillas', PlantillasWhatsappController.getPlantillasWhatsapp);
routes.put('/actualizar-plantilla/:idPlantilla', PlantillasWhatsappController.actualizarPlantillaWhatsapp);
routes.put('/actualizar-estado/:idPlantilla', PlantillasWhatsappController.actualizarEstado);
routes.get('/tipo-subareas', PlantillasWhatsappController.getTipoSubarea);

export default routes;
