import { Request, Response} from "express";
import {getRepository} from "typeorm";
import {PlantillaComercialWhatsapp} from "../../../entity/whatsapp/PlantillasComercialWhatsapp";
import {TipoSubarea} from "../../../entity/whatsapp/tipoSubarea";
import {isNull} from "util";
import {TipoSubareaPlantillaWhatsappView} from "../../../entity/whatsapp/TipoSubareaPlantillaWhatsappView";
import {TipoSubareaPlantillaWhatsapp} from "../../../entity/whatsapp/TipoSubareaPlantillaWhatsapp";

class PlantillasWhatsappController {
    static nuevaPlantilla = async (req: Request, res: Response) => {
        const arrIdTipoSubarea = req.body.idTipoSubarea;
        try {
            if (isNull(req.body.botonesData.tipoAccion)) {
                req.body.botonesData = null;
            }
            const plnatillasComercialData = req.body;
            // plnatillasComercialData.idTipoSubarea = 1;

            const dataComercialPlantilla = await getRepository(PlantillaComercialWhatsapp).save(plnatillasComercialData);

            let dataTipoSubareaPlantillas;
            for (let i = 0; i < arrIdTipoSubarea.length; i++) {
                dataTipoSubareaPlantillas = {
                    idTipoSubarea:  arrIdTipoSubarea[i],
                    idPlantillaComercialWhatsapp: dataComercialPlantilla.id,
                    activo: 1,
                };
                console.log(dataTipoSubareaPlantillas, 'dataTipoSubareaPlantillas')
                await getRepository(TipoSubareaPlantillaWhatsapp).save(dataTipoSubareaPlantillas);
            }

            res.status(200).json({message: "Datos guardado", data: dataComercialPlantilla});
        } catch (e) {
            console.log(e);
            res.status(500).json({error: e});
        }
    };

    static getTipoSubarea = async (req: Request, res: Response) => {

        const tipoSubareaRepository = getRepository(TipoSubarea);

        try {
            const tipoSubareaData = await tipoSubareaRepository.find().catch(e => {
                manejoErrores('No se encontró datos de tipo subarea', e, 404);
            });
            res.status(200).json(tipoSubareaData);
        } catch (e) {
            res.status(500).send("request not found");
        }
    };

    static getPlantillaWhatsapp = async (req: Request, res: Response) => {
        const idPlantilla = req.params.idPlantilla;
        try {
            const getComercialPlantilla = await getRepository(PlantillaComercialWhatsapp).findOne({
                where: { id: idPlantilla }
            });
            res.status(200).json({data: getComercialPlantilla});
        } catch (e) {
            res.status(500).json({error: e});
        }
    };

    static getPlantillasWhatsapp = async (req: Request, res: Response) => {
        try {
            const getComercialPlantilla = await getRepository(PlantillaComercialWhatsapp).find();
            res.status(200).json({data: getComercialPlantilla});
        } catch (e) {
            res.status(500).json({error: e});
        }
    };

    static actualizarPlantillaWhatsapp = async (req: Request, res: Response) => {
        const idPlantilla = req.params.idPlantilla;
        const dataPlantilla = req.body;
        const tipoSubareaData = req.body.tipoSubareaData;
        const tipoSubareaActivar = req.body.tipoSubareaActivar;
        const arrTiposubareaNuevo = req.body.arrTiposubareaNuevo;
        try {
            // const getComercialPlantilla = await getRepository(PlantillaComercialWhatsapp).findOne({
            //     where: {id: idPlantilla}
            // });
            let plantillasData = new PlantillaComercialWhatsapp();
            plantillasData.botonesData = JSON.stringify(dataPlantilla.botonesData);
            plantillasData.nombrePlantilla = dataPlantilla.nombrePlantilla;
            plantillasData.categoria = dataPlantilla.categoria;
            plantillasData.lenguaje = dataPlantilla.lenguaje;
            plantillasData.tipoEncabezado = dataPlantilla.tipoEncabezado;
            plantillasData.encabezado = dataPlantilla.encabezado;
            plantillasData.piePagina = dataPlantilla.piePagina;
            plantillasData.idTipoSubarea = dataPlantilla.idTipoSubarea;
            plantillasData.boton = dataPlantilla.boton;
            plantillasData.cuerpo = dataPlantilla.cuerpo;
// checar
            let dataTipoSubareaPlantillas;
            const AlltipoSubarea = await getRepository(TipoSubareaPlantillaWhatsapp).find({
                where: {
                    idPlantillaComercialWhatsapp: idPlantilla,
                }
            });
            // desactiva plantillas cambiando a 0
            if (tipoSubareaData) {
                for (let i = 0; i < tipoSubareaData.length; i++) {
                    const registroActualizar = await getRepository(TipoSubareaPlantillaWhatsapp).find({
                        where: {
                            idTipoSubarea: tipoSubareaData[i],
                            activo: 1,
                            idPlantillaComercialWhatsapp: idPlantilla
                        }
                    });
                    // console.log(registroActualizar, 'cambiando a 0');
                    if (registroActualizar.length !== 0) {
                        registroActualizar[0].activo = 0;
                        await getRepository(TipoSubareaPlantillaWhatsapp).update(registroActualizar[0].id, registroActualizar[0]);
                    }
                }
            }
            // activa plantillas cambiando a 1
            if (tipoSubareaActivar) {
                for (let i = 0; i < tipoSubareaActivar.length; i++) {
                    const registroActualizar = await getRepository(TipoSubareaPlantillaWhatsapp).find({
                        where: {
                            idTipoSubarea: tipoSubareaActivar[i],
                            activo: 0,
                            idPlantillaComercialWhatsapp: idPlantilla
                        }
                    });
                    // console.log(registroActualizar, 'actualizando a 1');
                    if (registroActualizar[0]) {
                        registroActualizar[0].activo = 1;
                        getRepository(TipoSubareaPlantillaWhatsapp).update(registroActualizar[0].id, registroActualizar[0]);
                    }
                }
            }
            //Se agrega nuevo tipoSubarea
            if (arrTiposubareaNuevo) {
                let dataTipoSubareaPlantillas;
                for (let i = 0; i < arrTiposubareaNuevo.length; i++) {
                    const registroActualizar = await getRepository(TipoSubareaPlantillaWhatsapp).find({
                        where: {
                            idTipoSubarea: arrTiposubareaNuevo[i],
                            activo: 0,
                            idPlantillaComercialWhatsapp: idPlantilla
                        }
                    });
                    console.log(registroActualizar, 'registroActualizarregistroActualizar');
                    if (!registroActualizar[0]) {
                        dataTipoSubareaPlantillas = {
                            idTipoSubarea:  arrTiposubareaNuevo[i],
                            idPlantillaComercialWhatsapp: idPlantilla,
                            activo: 1,
                        };
                        await getRepository(TipoSubareaPlantillaWhatsapp).save(dataTipoSubareaPlantillas);
                    } else {
                        registroActualizar[0].activo = 1;
                        getRepository(TipoSubareaPlantillaWhatsapp).update(registroActualizar[0].id, registroActualizar[0]);
                    }
                }
            }
            /*console.log('Cambiando a 1 los que están en input');
            for (let i = 0; i < dataPlantilla.idTipoSubarea.length; i++) {
                const tipoUpdate = await getRepository(TipoSubareaPlantillaWhatsapp).findOne({
                    where: {
                        idPlantillaComercialWhatsapp: idPlantilla,
                        idTipoSubarea:  dataPlantilla.idTipoSubarea[i],
                    }
                });
                tipoUpdate.activo = 1;
                await getRepository(TipoSubareaPlantillaWhatsapp).update(tipoUpdate.id, tipoUpdate);
            }*/

            await getRepository(PlantillaComercialWhatsapp).update(idPlantilla, plantillasData).then( () => {
                res.status(200).json('Se actualizaron los datos');
            });
        } catch (e) {
            console.log(e);
            res.status(500).json({error: e});
        }
    };

    static actualizarEstado = async (req: Request, res: Response) => {
        const idPlantilla = req.params.idPlantilla;
        const dataPlantilla = req.body;
        try {
            await getRepository(PlantillaComercialWhatsapp).update(idPlantilla, dataPlantilla).then( () => {
                res.status(200).json('Se actualizaron los datos');
            });
        } catch (e) {
            res.status(500).json({error: e});
        }
    };
}

// Functions
function manejoErrores(mensaje, res, codigo, cuerpo?) {
    res.send({
        cuerpo: String(cuerpo),
        mensaje: mensaje,
        codigo: codigo
    })
}

export default PlantillasWhatsappController;
