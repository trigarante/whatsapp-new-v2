import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class SubareasPlantillaActivaView {
    @PrimaryGeneratedColumn()
    idTipoSubarea: number;

    @Column()
    activo: number;

    @Column()
    idTipoPlantilla: number;

    @Column()
    subarea: string;

    @Column()
    idSubarea: number;
}
