import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class TipoSubareaPlantillaWhatsapp {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    idTipoSubarea: number;

    @Column()
    idPlantillaComercialWhatsapp: number;

    @Column()
    activo: number;
}
