import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class PlantillaComercialWhatsapp {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nombrePlantilla: string;

    @Column()
    cuerpo: string;

    @Column()
    categoria: string;

    @Column()
    lenguaje: string;

    @Column()
    encabezado: string;

    @Column()
    tipoEncabezado: string;

    @Column()
    piePagina: string;

    @Column()
    activa: number;

    @Column()
    fechaRegistro: Date;

    @Column()
    boton: string;

    @Column()
    botonesData: string;

    @Column({type: "json"})
    idTipoSubarea: any;
}
