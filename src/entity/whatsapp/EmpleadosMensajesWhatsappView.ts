import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class EmpleadosMensajesWhatsappView {
    @PrimaryGeneratedColumn()
    idEmpleado: number;

    @Column()
    idPuesto: number;

    @Column()
    puesto: string;

    @Column()
    nombre: string;

    @Column()
    paterno: string;

    @Column()
    materno: string;

    @Column()
    idTipoSubarea: number;

    @Column()
    idMensajesWhatsapp: number;

    @Column()
    idCanalWhatsapp: number;

    @Column()
    idEmpleadoEnvio: number;

    @Column()
    mensajes: string;

    @Column()
    fecha: Date;

    @Column()
    bandeja: number;

    @Column()
    correlationId: number;

    @Column()
    idTipoEnvio: number;
}
