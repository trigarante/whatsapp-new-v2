import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class mensajesWhatsapp {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    idCanalWhatsapp: number;

    @Column()
    idTipoMensajeWhatsapp: number;

    @Column()
    fecha: Date;

    @Column({type: "json"})
    mensaje: any;

    @Column({nullable: true})
    idEmpleadoEnvio: number;

    @Column()
    idTipoEnvio: number;

    @Column()
    correlationId: number;

    @Column()
    fechaCreacion: Date;

    @Column()
    aBandeja: number;

    @Column()
    idEmpleadoMensajes: number;
}
