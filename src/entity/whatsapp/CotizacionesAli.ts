import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class CotizacionesAli {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    idCotizacion: number;

    @Column()
    idSubRamo: number;

    @Column({type: "json"})
    peticion: any;

    @Column({type: "json"})
    respuesta: any;

    @Column()
    fechaCotizacion: Date;

    @Column()
    fechaActualizacion: Date;
}
