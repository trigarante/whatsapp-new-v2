import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class TipoSubarea {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    tipo: string;

    @Column()
    activo: number;
}
