import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class TipoSubareaPlantillaWhatsappView {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nombrePlantilla: string;

    @Column()
    cuerpo: string;

    @Column()
    activa: number;

    @Column()
    fechaRegistro: Date;

    @Column()
    categoria: string;

    @Column()
    lenguaje: string;

    @Column()
    tipoEncabezado: string;

    @Column()
    encabezado: string;

    @Column()
    piePagina: string;

    @Column()
    boton: string;

    @Column()
    botonesData: string;

    @Column()
    idTipoSubarea: number;

    @Column()
    activo: number;

    @Column()
    idTipoPlantilla: number;
}
