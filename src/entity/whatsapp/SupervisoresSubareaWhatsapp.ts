import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class SupervisoresSubareaWhatsappView {
    @PrimaryGeneratedColumn()
    idEmpleado: number;

    @Column()
    idUsuario: number;

    @Column()
    idPuesto: number;

    @Column()
    idSubarea: number;

    @Column()
    subarea: string;

    @Column()
    idTipoSubarea: number;
}
