import {Column, Entity, EntitySchema, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class tipoPlantilla {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    descripcion: string;

    @Column()
    activo: number;
}
