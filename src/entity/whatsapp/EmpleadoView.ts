import {Column, Entity, EntitySchema, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class empleadoView {
    @PrimaryGeneratedColumn()
    id: number;
    @Column()
    idBanco: number;
    @Column()
    idCandidato: number;
    @Column()
    idPuesto: number;
    @Column()
    idTipoPuesto: number;
    @Column()
    idSubarea: number;
    @Column()
    idUsuario: number;
    @Column()
    puestoDetalle: string;
    @Column()
    tipo: string;
    @Column()
    imagenEmpleado: string;
    @Column()
    recontratables: string;
    @Column()
    fechaAltaIMSS: Date;
    @Column()
    documentosPersonales: string;
    @Column()
    documentosAdministrativos: string;
    @Column()
    fechaIngreso: Date;
    @Column()
    sueldoDiario: number;
    @Column()
    sueldoMensual: number;
    @Column()
    kpi: number;
    @Column()
    fechaCambioSueldo: Date;
    @Column()
    ctaClabe: string;
    @Column()
    fechaAsignacion: Date;
    @Column()
    kpiMensual: number;
    @Column()
    kpiTrimestral: number;
    @Column()
    kpiSemestral: number;
    @Column()
    fechaRegistro: Date;
    @Column()
    comentarios: string;
    @Column()
    banco: string;
    @Column()
    idArea: number;
    @Column()
    subarea: string;
    @Column()
    idSede: number;
    @Column()
    area: string;
    @Column()
    idEmpresa: number;
    @Column()
    sede: string;
    @Column()
    idGrupo: number;
    @Column()
    empresa: string;
    @Column()
    grupo: string;
    @Column()
    puesto: string;
    @Column()
    puestoTipo: string;
    @Column()
    idPrecandidato: number;
    @Column()
    nombre: string;
    @Column()
    apellidoPaterno: string;
    @Column()
    apellidoMaterno: string;
    @Column()
    fechaNacimiento: Date;
    @Column()
    email: string;
    @Column()
    genero: string;
    @Column()
    idestadoCivil: number;
    @Column()
    idEscolaridad: number;
    @Column()
    idEstadoEscolaridad: number;
    @Column()
    cp: string;
    @Column()
    idColonia: number;
    @Column()
    colonia: string;
    @Column()
    calle: string;
    @Column()
    numeroExterior: string;
    @Column()
    numeroInterior: string;
    @Column()
    telefonoFijo: string;
    @Column()
    telefonoMovil: string;
    @Column()
    idEstadoRH: number;
    @Column()
    fechaCreacion: Date;
    @Column()
    fechaBaja: Date;
    @Column()
    tiempoTraslado: number;
    @Column()
    idMedioTraslado: number;
    @Column()
    idEstacion: number;
    @Column()
    estadoCivil: string;
    @Column()
    escolaridad: string;
    @Column()
    estadoEscolar: string;
    @Column()
    estadoRH: string;
    @Column()
    medio: string;
    @Column()
    estacion: string;
    @Column()
    lineas: string;
    @Column()
    idEtapa: number;
    @Column()
    curp: string;
    @Column()
    recontratable: string;
    @Column()
    detalleBaja: string;
    @Column()
    fechaRegistroBaja: Date;
    @Column()
    nombreEtapa: string;
    @Column()
    imss: string;
    @Column()
    rfc: string;
    @Column()
    idRazonSocial: number;
    @Column()
    idEstadoFiniquito: number;
    @Column()
    fechaFiniquito: Date;
    @Column()
    montoFiniquito: number;
    @Column()
    usuario: string;
    @Column()
    idTurnoEmpleado: number;
    @Column()
    turno: string;
    @Column()
    horario: string;
    @Column()
    tarjetaSecundaria: string;
    @Column()
    idTipoSubarea: number;
    @Column()
    idEstadoSolicitudBaja: number;
}
