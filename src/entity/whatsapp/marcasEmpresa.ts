import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class MarcasEmpresa {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    descripcion: string;

    @Column()
    activo: number;

    @Column()
    imagenPlantilla: string;
}
