import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class estadoCanalWhatsapp {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    estadoCanal: string;
}
