import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class canalWhatsapp {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    idSolicitud: number;

    @Column()
    destino: number;

    @Column()
    origen: string;

    @Column()
    fechaEnvio: Date;

    @Column()
    idEstadoCanal: number;

    @Column({type: "json"})
    respuestaMensajesMo: any;

    @Column()
    cantidadMo: number;

    @Column({type: "json"})
    numerosRestantes: any;

    @Column()
    activacion: number;

    @Column()
    idSocio: number;

    @Column()
    idMarcasEmpresa: number;
}
