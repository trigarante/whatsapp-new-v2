import { Socket } from 'socket.io'
import socketIO from 'socket.io'
import * as IO from "../../index";

// escuchar mensajes
export const mensaje = (data: Socket, io: socketIO.Server, idSolicitud, msgWavy?) => {
    data.on('chat-room', ( msg ) => {
        console.log('mensaje recibido', msg, idSolicitud);
        if (+msg === +idSolicitud) {
            io.emit(`mensaje-nuevo${msg}`, msgWavy);
        }
        // io.emit('mensaje-nuevo165278', 'Se la comeeeeeee el marioooooo');
        // io.to(`room_solicitudes_5032`).emit(`msg-solicitudes-5034`, { idCanalWhatsapp: 3,
        //     idEmpleadoEnvio: null,
        //     idTipoEnvio: 2,
        //     idTipoMensajeWhatsapp: '1',
        //     mensaje: 'wavy!!',
        //     fecha: '2021-04-15T23:02:09.952Z',
        //     correlationId: '1643512',
        //     aBandeja: 1,
        //     idEmpleadoMensajes: 6054,
        //     id: 11,
        //     tipoMensaje: 'TEXT',
        //     notificacion: 1,
        //     cantidadMensajesIndividuales: { '156793': 1, '1638692': 1, '1643512': 5 },
        // socketNumero: 4,
        //     solicitudDestino: '1643512' });
        // io.in(`${socketID}`).emit('mensaje-nuevo', msg);

        // emite mensaje a la sala a la que se haya ingresado
        // io.to(`room_${msg.idSolicitud}`).emit('mensaje-nuevo', msg.mensaje);
    });
};

export const desconectado = (data: Socket) => {
    data.on('disconnect', () => {
        console.log('desconectado');
    });
};
